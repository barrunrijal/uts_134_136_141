package barrun.rijal.uts_136_134_141

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel


class MainViewModel : ViewModel() {
    private  val repo = repo()
    fun fetcUserData() : LiveData<MutableList<isi_row>>{
        val mutableData = MutableLiveData<MutableList<isi_row>>()
        repo.getData().observeForever { databar ->
            mutableData.value = databar
        }
        return  mutableData
    }
}
