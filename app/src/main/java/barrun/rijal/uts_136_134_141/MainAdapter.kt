package barrun.rijal.uts_136_134_141

import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.item_row.view.*

class MainAdapter(private val context: Context, val data : MainActivity) : RecyclerView.Adapter<MainAdapter.MainViewHolder>() {
    private var dataList = mutableListOf<isi_row>()

    fun setListData(data: MutableList<isi_row>) {
        dataList = data
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MainViewHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.item_row, parent, false)
        return MainViewHolder(view)
    }

    override fun getItemCount(): Int {
        return if (dataList.size > 0) {
            dataList.size
        } else {
            0
        }
    }

    override fun onBindViewHolder(holder: MainViewHolder, position: Int) {
        val user = dataList[position]
        holder.bindview(user)


        if (position.rem(2) == 0) holder.lin.setBackgroundColor(Color.rgb(86, 138, 255))
        else holder.lin.setBackgroundColor(Color.rgb(64, 196, 245))


        holder.card.setOnClickListener {
            data.isis.setText(user.keterangan)
            val det = Intent(this.data, kelola_data::class.java)
            det.putExtra("id", user.id)
            det.putExtra("nama", user.nama)
            det.putExtra("harga", user.harga)
            det.putExtra("imageurl", user.imageUrl)
            det.putExtra("stock", user.jenis)
            det.putExtra("status", user.status)
            det.putExtra("keterangan", user.keterangan)
            this.data.startActivity(det)
        }
    }

    inner class MainViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bindview(user: isi_row) {
            Glide.with(context).load(user.imageUrl).into(itemView.foto)
            itemView.nama.text = user.nama
            itemView.harga.text = "Rp." + user.harga
            itemView.stoc.text = user.jenis
            itemView.status.text = user.status
        }

        val card = itemView.isian
        val lin = itemView.war
    }
}
