package barrun.rijal.uts_136_134_141

import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import cn.pedant.SweetAlert.SweetAlertDialog
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.activity_login.*


class LoginActivity : AppCompatActivity(), View.OnClickListener {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        login.setOnClickListener(this)
        regis.setOnClickListener(this)

    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.login -> {
                var mail = email.text.toString()
                var pass = txpassword.text.toString()
                if (mail.isEmpty() || pass.isEmpty()) {
                    email.error = "Tidak Boleh Kosong"
                    txpassword.error = "Tidak Boleh Kosong"
                } else {
                    val prDialog = SweetAlertDialog(this, SweetAlertDialog.PROGRESS_TYPE)
                    prDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"))
                    prDialog.setTitleText("Loging....")
                    prDialog.setCancelable(false)
                    prDialog.show()
                    FirebaseAuth.getInstance().signInWithEmailAndPassword(mail, pass)
                        .addOnCompleteListener {
                            if (!it.isSuccessful) return@addOnCompleteListener
                            prDialog.hide()
                            sukses("$mail")
                            email.setText("")
                            txpassword.setText("")

                        }
                        .addOnFailureListener {
                            prDialog.hide()
                            alert("Chek Email & Password")
                        }
                }

            }
            R.id.regis -> {
                val intent = Intent(this, RegisterActivity::class.java)
                startActivity(intent)

            }
        }

    }

    fun alert(string: String?) {
        val allAlert = SweetAlertDialog(this, SweetAlertDialog.ERROR_TYPE)
        allAlert.setCancelable(false)
        allAlert.setTitleText("Maaf")
            .setContentText(string)
            .show()
    }

    fun sukses(string: String?) {
        SweetAlertDialog(this, SweetAlertDialog.SUCCESS_TYPE)
            .setTitleText("Selamat Datang")
            .setContentText(string)
            .setConfirmText("OK")
            .setConfirmClickListener { sDialog -> sDialog.dismissWithAnimation()
                val intent = Intent(this, MenuActivity::class.java)
                startActivity(intent)
            }
            .show()
    }
}
